﻿namespace NightLifeX
{
    partial class gui_topsales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gui_topsales));
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl1_spt_1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.slider_img_1 = new System.Windows.Forms.PictureBox();
            this.pnl1_lbl_greeting = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 163);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(983, 419);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnl1_spt_1);
            this.panel1.Controls.Add(this.slider_img_1);
            this.panel1.Controls.Add(this.pnl1_lbl_greeting);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(983, 163);
            this.panel1.TabIndex = 2;
            // 
            // pnl1_spt_1
            // 
            this.pnl1_spt_1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_spt_1.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_spt_1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.pnl1_spt_1.LineThickness = 1;
            this.pnl1_spt_1.Location = new System.Drawing.Point(213, 128);
            this.pnl1_spt_1.Name = "pnl1_spt_1";
            this.pnl1_spt_1.Size = new System.Drawing.Size(568, 35);
            this.pnl1_spt_1.TabIndex = 19;
            this.pnl1_spt_1.Transparency = 255;
            this.pnl1_spt_1.Vertical = false;
            // 
            // slider_img_1
            // 
            this.slider_img_1.BackColor = System.Drawing.Color.Transparent;
            this.slider_img_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.slider_img_1.Image = ((System.Drawing.Image)(resources.GetObject("slider_img_1.Image")));
            this.slider_img_1.Location = new System.Drawing.Point(12, 9);
            this.slider_img_1.Name = "slider_img_1";
            this.slider_img_1.Size = new System.Drawing.Size(195, 140);
            this.slider_img_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.slider_img_1.TabIndex = 22;
            this.slider_img_1.TabStop = false;
            // 
            // pnl1_lbl_greeting
            // 
            this.pnl1_lbl_greeting.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_lbl_greeting.AutoSize = true;
            this.pnl1_lbl_greeting.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_lbl_greeting.Font = new System.Drawing.Font("Century Gothic", 72F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl1_lbl_greeting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.pnl1_lbl_greeting.Location = new System.Drawing.Point(213, 18);
            this.pnl1_lbl_greeting.Name = "pnl1_lbl_greeting";
            this.pnl1_lbl_greeting.Size = new System.Drawing.Size(573, 118);
            this.pnl1_lbl_greeting.TabIndex = 20;
            this.pnl1_lbl_greeting.Text = "+ Vendidos";
            // 
            // gui_topsales
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(983, 582);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "gui_topsales";
            this.Text = "gui_topsales";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuSeparator pnl1_spt_1;
        private System.Windows.Forms.Label pnl1_lbl_greeting;
        private System.Windows.Forms.PictureBox slider_img_1;
    }
}