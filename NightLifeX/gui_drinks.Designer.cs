﻿namespace NightLifeX
{
    partial class gui_drinks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gui_drinks));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl1_spt_1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.pnl1_lbl_greeting = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuTileButton6 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton5 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton4 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton3 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton2 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton1 = new Bunifu.Framework.UI.BunifuTileButton();
            this.pnl1_img_client = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl1_img_client)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.pnl1_img_client);
            this.panel1.Controls.Add(this.pnl1_spt_1);
            this.panel1.Controls.Add(this.pnl1_lbl_greeting);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(983, 163);
            this.panel1.TabIndex = 0;
            // 
            // pnl1_spt_1
            // 
            this.pnl1_spt_1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_spt_1.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_spt_1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.pnl1_spt_1.LineThickness = 1;
            this.pnl1_spt_1.Location = new System.Drawing.Point(184, 128);
            this.pnl1_spt_1.Name = "pnl1_spt_1";
            this.pnl1_spt_1.Size = new System.Drawing.Size(568, 35);
            this.pnl1_spt_1.TabIndex = 19;
            this.pnl1_spt_1.Transparency = 255;
            this.pnl1_spt_1.Vertical = false;
            // 
            // pnl1_lbl_greeting
            // 
            this.pnl1_lbl_greeting.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_lbl_greeting.AutoSize = true;
            this.pnl1_lbl_greeting.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_lbl_greeting.Font = new System.Drawing.Font("Century Gothic", 72F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl1_lbl_greeting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.pnl1_lbl_greeting.Location = new System.Drawing.Point(313, 33);
            this.pnl1_lbl_greeting.Name = "pnl1_lbl_greeting";
            this.pnl1_lbl_greeting.Size = new System.Drawing.Size(327, 118);
            this.pnl1_lbl_greeting.TabIndex = 20;
            this.pnl1_lbl_greeting.Text = "Drinks";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.bunifuTileButton6);
            this.panel2.Controls.Add(this.bunifuTileButton5);
            this.panel2.Controls.Add(this.bunifuTileButton4);
            this.panel2.Controls.Add(this.bunifuTileButton3);
            this.panel2.Controls.Add(this.bunifuTileButton2);
            this.panel2.Controls.Add(this.bunifuTileButton1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 163);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(983, 419);
            this.panel2.TabIndex = 1;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuTileButton6
            // 
            this.bunifuTileButton6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuTileButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.bunifuTileButton6.color = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.bunifuTileButton6.colorActive = System.Drawing.Color.DimGray;
            this.bunifuTileButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuTileButton6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTileButton6.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton6.Image")));
            this.bunifuTileButton6.ImagePosition = -15;
            this.bunifuTileButton6.ImageZoom = 100;
            this.bunifuTileButton6.LabelPosition = 38;
            this.bunifuTileButton6.LabelText = "Champagne";
            this.bunifuTileButton6.Location = new System.Drawing.Point(299, 200);
            this.bunifuTileButton6.Margin = new System.Windows.Forms.Padding(7);
            this.bunifuTileButton6.Name = "bunifuTileButton6";
            this.bunifuTileButton6.Size = new System.Drawing.Size(142, 135);
            this.bunifuTileButton6.TabIndex = 25;
            // 
            // bunifuTileButton5
            // 
            this.bunifuTileButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton5.color = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton5.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.bunifuTileButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton5.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 18F);
            this.bunifuTileButton5.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton5.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton5.Image")));
            this.bunifuTileButton5.ImagePosition = 0;
            this.bunifuTileButton5.ImageZoom = 95;
            this.bunifuTileButton5.LabelPosition = 50;
            this.bunifuTileButton5.LabelText = "Tequila";
            this.bunifuTileButton5.Location = new System.Drawing.Point(66, 200);
            this.bunifuTileButton5.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton5.Name = "bunifuTileButton5";
            this.bunifuTileButton5.Size = new System.Drawing.Size(142, 135);
            this.bunifuTileButton5.TabIndex = 25;
            // 
            // bunifuTileButton4
            // 
            this.bunifuTileButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTileButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton4.color = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton4.colorActive = System.Drawing.Color.Gainsboro;
            this.bunifuTileButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton4.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 18F);
            this.bunifuTileButton4.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton4.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton4.Image")));
            this.bunifuTileButton4.ImagePosition = -17;
            this.bunifuTileButton4.ImageZoom = 100;
            this.bunifuTileButton4.LabelPosition = 49;
            this.bunifuTileButton4.LabelText = "Brandy";
            this.bunifuTileButton4.Location = new System.Drawing.Point(767, 9);
            this.bunifuTileButton4.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton4.Name = "bunifuTileButton4";
            this.bunifuTileButton4.Size = new System.Drawing.Size(142, 135);
            this.bunifuTileButton4.TabIndex = 24;
            // 
            // bunifuTileButton3
            // 
            this.bunifuTileButton3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuTileButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton3.color = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton3.colorActive = System.Drawing.Color.Khaki;
            this.bunifuTileButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton3.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 18F);
            this.bunifuTileButton3.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton3.Image")));
            this.bunifuTileButton3.ImagePosition = -11;
            this.bunifuTileButton3.ImageZoom = 85;
            this.bunifuTileButton3.LabelPosition = 49;
            this.bunifuTileButton3.LabelText = "Whiskey";
            this.bunifuTileButton3.Location = new System.Drawing.Point(532, 9);
            this.bunifuTileButton3.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton3.Name = "bunifuTileButton3";
            this.bunifuTileButton3.Size = new System.Drawing.Size(142, 135);
            this.bunifuTileButton3.TabIndex = 23;
            // 
            // bunifuTileButton2
            // 
            this.bunifuTileButton2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuTileButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton2.color = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton2.colorActive = System.Drawing.Color.LightSalmon;
            this.bunifuTileButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton2.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 18F);
            this.bunifuTileButton2.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton2.Image")));
            this.bunifuTileButton2.ImagePosition = 0;
            this.bunifuTileButton2.ImageZoom = 75;
            this.bunifuTileButton2.LabelPosition = 49;
            this.bunifuTileButton2.LabelText = "Ron";
            this.bunifuTileButton2.Location = new System.Drawing.Point(299, 9);
            this.bunifuTileButton2.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton2.Name = "bunifuTileButton2";
            this.bunifuTileButton2.Size = new System.Drawing.Size(142, 135);
            this.bunifuTileButton2.TabIndex = 22;
            // 
            // bunifuTileButton1
            // 
            this.bunifuTileButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton1.color = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.bunifuTileButton1.colorActive = System.Drawing.Color.PaleTurquoise;
            this.bunifuTileButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton1.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 18F);
            this.bunifuTileButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton1.Image")));
            this.bunifuTileButton1.ImagePosition = -6;
            this.bunifuTileButton1.ImageZoom = 80;
            this.bunifuTileButton1.LabelPosition = 49;
            this.bunifuTileButton1.LabelText = "Vodka";
            this.bunifuTileButton1.Location = new System.Drawing.Point(66, 9);
            this.bunifuTileButton1.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton1.Name = "bunifuTileButton1";
            this.bunifuTileButton1.Size = new System.Drawing.Size(142, 135);
            this.bunifuTileButton1.TabIndex = 21;
            // 
            // pnl1_img_client
            // 
            this.pnl1_img_client.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_img_client.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnl1_img_client.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.pnl1_img_client.Image = ((System.Drawing.Image)(resources.GetObject("pnl1_img_client.Image")));
            this.pnl1_img_client.ImageActive = null;
            this.pnl1_img_client.Location = new System.Drawing.Point(165, 37);
            this.pnl1_img_client.Name = "pnl1_img_client";
            this.pnl1_img_client.Size = new System.Drawing.Size(171, 103);
            this.pnl1_img_client.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pnl1_img_client.TabIndex = 21;
            this.pnl1_img_client.TabStop = false;
            this.pnl1_img_client.Zoom = 10;
            // 
            // gui_drinks
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(983, 582);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "gui_drinks";
            this.Text = "gui_drinks";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnl1_img_client)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuSeparator pnl1_spt_1;
        private System.Windows.Forms.Label pnl1_lbl_greeting;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton6;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton5;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton4;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton3;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton2;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton1;
        private Bunifu.Framework.UI.BunifuImageButton pnl1_img_client;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
    }
}