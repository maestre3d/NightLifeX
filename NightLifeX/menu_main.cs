﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Diagnostics;
using nightlifex_Controller;
// NightLifeX®

namespace NightLifeX
{
    public partial class gui_root : Form
    {
        //  Min-Max Flag
        static bool maxB = false;

        bool flHome;
        static object formChild;


        /*
        ClsContacto oContact = null;
        ClsContactoMgr oContactMgr = null;
        DataTable Dtt = null;*/

        public gui_root()
        {
            InitializeComponent();
            //lbl_date.Text = DateTime.Now.ToString("dddd - dd/MMMM/yyyy");
            slider_img_n_min.Hide();
            gui_top_11.Hide();
            gui_uc_drinks1.Hide();
            slider_btn_1.Normalcolor = slider_btn_1.Activecolor;
        }



        // Create Form's child
        private void createChild()
        {
            Form fh = formChild as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.gui_card.Controls.Add(fh);
            this.gui_card.Tag = fh;
            fh.Show();
        }


        //  Show Main Menu
        private void b_menu_Click(object sender, EventArgs e)
        {
            flHome = false;
            main_panel.Show();
            gui_top_11.SendToBack();
            gui_top_11.Hide();
            gui_uc_drinks1.SendToBack();
            gui_uc_drinks1.Hide();
            slider_btn_2.Normalcolor = slider_btn_1.Normalcolor;
            slider_btn_1.Normalcolor = slider_btn_1.Activecolor;

        }

        //  Show Drinks Menu
        private void b_drinks_Click(object sender, EventArgs e)
        {
            flHome = true;
            slider_btn_1.Normalcolor = slider_btn_2.Normalcolor;
            slider_btn_2.Normalcolor = slider_btn_2.Activecolor;
            gui_uc_drinks1.Show();
            gui_uc_drinks1.BringToFront();
        }

        // Show + Vendidos
        private void main_img_1_Click(object sender, EventArgs e)
        {
            flHome = true;
            
            gui_top_11.Show();
            gui_top_11.BringToFront();
        }

        // --------


        //  Slider Attributes
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            // Small activated
            if (slider_pnl.Width == 75)
            {
                slider_pnl.Visible = false;
                slider_pnl.Width = 255;

                slider_img_n_min.Show();
                slider_img_powered.Show();
                slider_div_1.Show();
                slider_div_2.Show();
                slider_img_night.Show();
                slider_img_n_min.Hide();


                slider_anim2.ShowSync(slider_pnl);

            }

            // Big Activated
            else
            {
                slider_pnl.Visible = false;
                slider_img_night.Hide();
                slider_img_n_min.Show();
                slider_img_powered.Hide();
                slider_div_1.Hide();
                slider_div_2.Hide();


                slider_pnl.Width = 75;
                slider_anim1.ShowSync(slider_pnl);
            }
        }

        //  Close App
        private void b_off_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //  Maximize App
        private void b_max_Click(object sender, EventArgs e)
        {
            if (maxB == false)
            {
                this.WindowState = FormWindowState.Maximized;
                // +88  per Y axis
                // +200 per X axis
                //this.main_img_1.Location = new Point(92, 332);
                //this.main_txt_1.Location = new Point(133, 547);
                //this.main_img_2.Location = new Point(693, 532);
                //this.main_txt_2.Location = new Point(752, 747);
                //this.main_img_3.Location = new Point(1292, 332);
                //this.main_txt_3.Location = new Point(1347, 547);

                act_main_anims();
                maxB = true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                //this.main_img_1.Location = new Point(92, 244);
                //this.main_txt_1.Location = new Point(133, 459);
                this.main_img_2.Location = new Point(393, 244);
                this.main_txt_2.Location = new Point(452, 459);
                //this.main_img_3.Location = new Point(692, 244);
                //this.main_txt_3.Location = new Point(747, 459);

                act_main_anims();
                maxB = false;
            }
        }

        //  Minimize App
        private void b_min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        //  Open Client's Facebook/Webpage
        private void img_client_bottom_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.facebook.com/PriveCuu/");
        }

        private void act_main_anims()
        {
            if (flHome == false)
            {
                // Anims
                main_img_1.Visible = false;
                main_txt_1.Visible = false;
                main_img_2.Visible = false;
                main_txt_2.Visible = false;
                main_img_3.Visible = false;
                main_txt_3.Visible = false;

                anim_vslide.ShowSync(main_img_1);
                anim_vslide.ShowSync(main_txt_1);
                anim_fade.ShowSync(main_img_2);
                anim_fade.ShowSync(main_txt_2);
                anim_alt.ShowSync(main_img_3);
                anim_alt.ShowSync(main_txt_3);
            }

        }

        private void slider_img_powered_Click(object sender, EventArgs e)
        {
            gui_extra.about fAbout = new gui_extra.about();
            fAbout.Show();
        }

        private void userControl11_Load(object sender, EventArgs e)
        {

        }

        private void slider_btn_4_Click(object sender, EventArgs e)
        {
            flHome = true;
            main_panel.Hide();
            createChild();
        }

        private void gui_top_11_Load(object sender, EventArgs e)
        {

        }

        private void gui_root_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
