﻿namespace NightLifeX.gui_extra
{
    partial class about
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(about));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnl_top = new System.Windows.Forms.Panel();
            this.pnl_fill = new System.Windows.Forms.Panel();
            this.drag_1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.drag_2 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.b_min = new Bunifu.Framework.UI.BunifuImageButton();
            this.b_off = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl_top.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.b_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_off)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // pnl_top
            // 
            this.pnl_top.Controls.Add(this.b_min);
            this.pnl_top.Controls.Add(this.b_off);
            this.pnl_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_top.Location = new System.Drawing.Point(0, 0);
            this.pnl_top.Name = "pnl_top";
            this.pnl_top.Size = new System.Drawing.Size(421, 30);
            this.pnl_top.TabIndex = 24;
            // 
            // pnl_fill
            // 
            this.pnl_fill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_fill.Location = new System.Drawing.Point(0, 30);
            this.pnl_fill.Name = "pnl_fill";
            this.pnl_fill.Size = new System.Drawing.Size(421, 362);
            this.pnl_fill.TabIndex = 25;
            // 
            // drag_1
            // 
            this.drag_1.Fixed = true;
            this.drag_1.Horizontal = true;
            this.drag_1.TargetControl = this.pnl_top;
            this.drag_1.Vertical = true;
            // 
            // drag_2
            // 
            this.drag_2.Fixed = true;
            this.drag_2.Horizontal = true;
            this.drag_2.TargetControl = this.pnl_fill;
            this.drag_2.Vertical = true;
            // 
            // b_min
            // 
            this.b_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_min.BackColor = System.Drawing.Color.Transparent;
            this.b_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b_min.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.b_min.Image = ((System.Drawing.Image)(resources.GetObject("b_min.Image")));
            this.b_min.ImageActive = null;
            this.b_min.Location = new System.Drawing.Point(363, 9);
            this.b_min.Name = "b_min";
            this.b_min.Size = new System.Drawing.Size(20, 21);
            this.b_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.b_min.TabIndex = 4;
            this.b_min.TabStop = false;
            this.b_min.Zoom = 10;
            this.b_min.Click += new System.EventHandler(this.b_min_Click_1);
            // 
            // b_off
            // 
            this.b_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_off.BackColor = System.Drawing.Color.Transparent;
            this.b_off.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b_off.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.b_off.Image = ((System.Drawing.Image)(resources.GetObject("b_off.Image")));
            this.b_off.ImageActive = null;
            this.b_off.Location = new System.Drawing.Point(389, 9);
            this.b_off.Name = "b_off";
            this.b_off.Size = new System.Drawing.Size(20, 21);
            this.b_off.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.b_off.TabIndex = 3;
            this.b_off.TabStop = false;
            this.b_off.Zoom = 10;
            this.b_off.Click += new System.EventHandler(this.b_off_Click_1);
            // 
            // about
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 392);
            this.Controls.Add(this.pnl_fill);
            this.Controls.Add(this.pnl_top);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "about";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "about";
            this.pnl_top.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.b_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_off)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel pnl_fill;
        private System.Windows.Forms.Panel pnl_top;
        private Bunifu.Framework.UI.BunifuImageButton b_min;
        private Bunifu.Framework.UI.BunifuImageButton b_off;
        private Bunifu.Framework.UI.BunifuDragControl drag_1;
        private Bunifu.Framework.UI.BunifuDragControl drag_2;
    }
}