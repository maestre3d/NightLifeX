﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NightLifeX.gui_extra
{
    public partial class about : Form
    {

        public about()
        {
            InitializeComponent();
        }

        //  Close App
        private void b_off_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        //  Minimize App
        private void b_min_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
