﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace NightLifeX
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new gui_boot());
            Thread t1 = new Thread( new ThreadStart(initBoot) );
            t1.Name = "Boot Launcher";
            Thread t2 = new Thread( new ThreadStart(initMenu) );
            t2.Name = "Main Menu";

            t1.IsBackground = true;

            t1.Start();
            Thread.Sleep(18600);
            t2.Start();

            //Application.Run(new gui_boot());
        }

        static void initBoot()
        {
            Application.Run(new gui_boot());
        }

        static void initMenu()
        {
            Application.Run(new gui_root());
        }
    }
}
