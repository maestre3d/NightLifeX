﻿namespace NightLifeX
{
    partial class gui_uc_drinks
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gui_uc_drinks));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl1_spt_1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.pnl1_img_client = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl1_lbl_greeting = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.main_txt_1 = new System.Windows.Forms.Label();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.main_img_1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.main_pnl_drag = new System.Windows.Forms.Panel();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl1_img_client)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnl1_img_client);
            this.panel1.Controls.Add(this.main_pnl_drag);
            this.panel1.Controls.Add(this.pnl1_spt_1);
            this.panel1.Controls.Add(this.pnl1_lbl_greeting);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(983, 144);
            this.panel1.TabIndex = 0;
            // 
            // pnl1_spt_1
            // 
            this.pnl1_spt_1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_spt_1.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_spt_1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.pnl1_spt_1.LineThickness = 1;
            this.pnl1_spt_1.Location = new System.Drawing.Point(217, 105);
            this.pnl1_spt_1.Name = "pnl1_spt_1";
            this.pnl1_spt_1.Size = new System.Drawing.Size(568, 35);
            this.pnl1_spt_1.TabIndex = 22;
            this.pnl1_spt_1.Transparency = 255;
            this.pnl1_spt_1.Vertical = false;
            // 
            // pnl1_img_client
            // 
            this.pnl1_img_client.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_img_client.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_img_client.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnl1_img_client.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.pnl1_img_client.Image = ((System.Drawing.Image)(resources.GetObject("pnl1_img_client.Image")));
            this.pnl1_img_client.ImageActive = null;
            this.pnl1_img_client.Location = new System.Drawing.Point(-40, 5);
            this.pnl1_img_client.Name = "pnl1_img_client";
            this.pnl1_img_client.Size = new System.Drawing.Size(251, 140);
            this.pnl1_img_client.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pnl1_img_client.TabIndex = 24;
            this.pnl1_img_client.TabStop = false;
            this.pnl1_img_client.Zoom = 10;
            // 
            // pnl1_lbl_greeting
            // 
            this.pnl1_lbl_greeting.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_lbl_greeting.AutoSize = true;
            this.pnl1_lbl_greeting.BackColor = System.Drawing.Color.Transparent;
            this.pnl1_lbl_greeting.Font = new System.Drawing.Font("Century Gothic", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl1_lbl_greeting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.pnl1_lbl_greeting.Location = new System.Drawing.Point(332, 0);
            this.pnl1_lbl_greeting.Name = "pnl1_lbl_greeting";
            this.pnl1_lbl_greeting.Size = new System.Drawing.Size(313, 115);
            this.pnl1_lbl_greeting.TabIndex = 23;
            this.pnl1_lbl_greeting.Text = "Drinks";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.AutoScrollMinSize = new System.Drawing.Size(966, 438);
            this.panel2.AutoSize = true;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Controls.Add(this.bunifuSeparator1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.bunifuImageButton3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.main_txt_1);
            this.panel2.Controls.Add(this.bunifuImageButton2);
            this.panel2.Controls.Add(this.bunifuImageButton1);
            this.panel2.Controls.Add(this.main_img_1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 144);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(983, 438);
            this.panel2.TabIndex = 1;
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(217, 805);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(568, 35);
            this.bunifuSeparator1.TabIndex = 22;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(135, 673);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 33);
            this.label3.TabIndex = 30;
            this.label3.Text = "Champagne";
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton3.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.ImageActive")));
            this.bunifuImageButton3.Location = new System.Drawing.Point(68, 391);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(278, 268);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 29;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 10;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(724, 327);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 33);
            this.label2.TabIndex = 28;
            this.label2.Text = "Vodka";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.label1.Location = new System.Drawing.Point(437, 327);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 33);
            this.label1.TabIndex = 28;
            this.label1.Text = "Whiskey";
            // 
            // main_txt_1
            // 
            this.main_txt_1.AutoSize = true;
            this.main_txt_1.BackColor = System.Drawing.Color.Transparent;
            this.main_txt_1.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.main_txt_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.main_txt_1.Location = new System.Drawing.Point(159, 327);
            this.main_txt_1.Name = "main_txt_1";
            this.main_txt_1.Size = new System.Drawing.Size(90, 33);
            this.main_txt_1.TabIndex = 28;
            this.main_txt_1.Text = "Tequila";
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton2.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.ImageActive")));
            this.bunifuImageButton2.Location = new System.Drawing.Point(619, 43);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(278, 268);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 25;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton1.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.ImageActive")));
            this.bunifuImageButton1.Location = new System.Drawing.Point(344, 43);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(278, 268);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 25;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            // 
            // main_img_1
            // 
            this.main_img_1.BackColor = System.Drawing.Color.Transparent;
            this.main_img_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.main_img_1.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.main_img_1.Image = ((System.Drawing.Image)(resources.GetObject("main_img_1.Image")));
            this.main_img_1.ImageActive = ((System.Drawing.Image)(resources.GetObject("main_img_1.ImageActive")));
            this.main_img_1.Location = new System.Drawing.Point(68, 43);
            this.main_img_1.Name = "main_img_1";
            this.main_img_1.Size = new System.Drawing.Size(278, 268);
            this.main_img_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.main_img_1.TabIndex = 25;
            this.main_img_1.TabStop = false;
            this.main_img_1.Zoom = 10;
            // 
            // main_pnl_drag
            // 
            this.main_pnl_drag.Dock = System.Windows.Forms.DockStyle.Top;
            this.main_pnl_drag.Location = new System.Drawing.Point(0, 0);
            this.main_pnl_drag.Name = "main_pnl_drag";
            this.main_pnl_drag.Size = new System.Drawing.Size(983, 20);
            this.main_pnl_drag.TabIndex = 25;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.main_pnl_drag;
            this.bunifuDragControl1.Vertical = true;
            // 
            // gui_uc_drinks
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "gui_uc_drinks";
            this.Size = new System.Drawing.Size(983, 582);
            this.Load += new System.EventHandler(this.gui_uc_drinks_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnl1_img_client)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuSeparator pnl1_spt_1;
        private Bunifu.Framework.UI.BunifuImageButton pnl1_img_client;
        private System.Windows.Forms.Label pnl1_lbl_greeting;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label main_txt_1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuImageButton main_img_1;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.Panel main_pnl_drag;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
    }
}
