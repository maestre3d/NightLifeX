﻿namespace NightLifeX
{
    partial class gui_root
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation7 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation6 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gui_root));
            BunifuAnimatorNS.Animation animation9 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation8 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation10 = new BunifuAnimatorNS.Animation();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.drag_slider = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.slider_pnl = new System.Windows.Forms.Panel();
            this.slider_img_1 = new System.Windows.Forms.PictureBox();
            this.slider_img_night = new System.Windows.Forms.PictureBox();
            this.slider_img_n_min = new System.Windows.Forms.PictureBox();
            this.slider_btn_1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.slider_btn_2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.slider_btn_logout = new Bunifu.Framework.UI.BunifuFlatButton();
            this.slider_btn_4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.slider_btn_3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.slider_pnl_3 = new System.Windows.Forms.Panel();
            this.slider_div_2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.slider_div_1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.slider_pnl_left = new System.Windows.Forms.Panel();
            this.slider_pnl_drinks = new System.Windows.Forms.Panel();
            this.slider_pnl_mmenu = new System.Windows.Forms.Panel();
            this.slider_pnl_logout = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.slider_img_powered = new System.Windows.Forms.PictureBox();
            this.drag_mainpnl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.drag_mainmenu = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.main_pnl_drag = new System.Windows.Forms.Panel();
            this.drag_pnl_bottom = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bottom_pnl = new System.Windows.Forms.Panel();
            this.bottom_txt_user = new System.Windows.Forms.Label();
            this.bottom_img_client = new Bunifu.Framework.UI.BunifuImageButton();
            this.slider_anim1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.main_panel = new System.Windows.Forms.Panel();
            this.main_txt_3 = new System.Windows.Forms.Label();
            this.main_txt_2 = new System.Windows.Forms.Label();
            this.main_txt_1 = new System.Windows.Forms.Label();
            this.main_img_3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.main_img_2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.main_img_1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl1_spt_1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.main_banner_txt_1 = new System.Windows.Forms.Label();
            this.main_banner_img_client = new Bunifu.Framework.UI.BunifuImageButton();
            this.gui_top_11 = new NightLifeX.gui_top_1();
            this.gui_uc_drinks1 = new NightLifeX.gui_uc_drinks();
            this.b_min = new Bunifu.Framework.UI.BunifuImageButton();
            this.b_max = new Bunifu.Framework.UI.BunifuImageButton();
            this.b_off = new Bunifu.Framework.UI.BunifuImageButton();
            this.gui_card = new Bunifu.Framework.UI.BunifuCards();
            this.slider_anim2 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.anim_vslide = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.anim_fade = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.anim_alt = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.slider_pnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_night)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_n_min)).BeginInit();
            this.slider_pnl_left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_powered)).BeginInit();
            this.bottom_pnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottom_img_client)).BeginInit();
            this.main_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_banner_img_client)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_off)).BeginInit();
            this.gui_card.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // drag_slider
            // 
            this.drag_slider.Fixed = true;
            this.drag_slider.Horizontal = true;
            this.drag_slider.TargetControl = this.slider_pnl;
            this.drag_slider.Vertical = true;
            // 
            // slider_pnl
            // 
            this.slider_pnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.slider_pnl.Controls.Add(this.slider_img_1);
            this.slider_pnl.Controls.Add(this.slider_img_night);
            this.slider_pnl.Controls.Add(this.slider_img_n_min);
            this.slider_pnl.Controls.Add(this.slider_btn_1);
            this.slider_pnl.Controls.Add(this.slider_btn_2);
            this.slider_pnl.Controls.Add(this.slider_btn_logout);
            this.slider_pnl.Controls.Add(this.slider_btn_4);
            this.slider_pnl.Controls.Add(this.slider_btn_3);
            this.slider_pnl.Controls.Add(this.slider_pnl_3);
            this.slider_pnl.Controls.Add(this.slider_div_2);
            this.slider_pnl.Controls.Add(this.slider_div_1);
            this.slider_pnl.Controls.Add(this.slider_pnl_left);
            this.slider_pnl.Controls.Add(this.slider_img_powered);
            this.anim_vslide.SetDecoration(this.slider_pnl, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_pnl, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_pnl, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_pnl, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_pnl, BunifuAnimatorNS.DecorationType.None);
            this.slider_pnl.Dock = System.Windows.Forms.DockStyle.Left;
            this.slider_pnl.Location = new System.Drawing.Point(0, 0);
            this.slider_pnl.Name = "slider_pnl";
            this.slider_pnl.Size = new System.Drawing.Size(255, 741);
            this.slider_pnl.TabIndex = 5;
            // 
            // slider_img_1
            // 
            this.slider_img_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.slider_img_1.BackColor = System.Drawing.Color.Transparent;
            this.slider_img_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.slider_img_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_img_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_img_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_img_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_img_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_img_1.Image = ((System.Drawing.Image)(resources.GetObject("slider_img_1.Image")));
            this.slider_img_1.Location = new System.Drawing.Point(203, 10);
            this.slider_img_1.Name = "slider_img_1";
            this.slider_img_1.Size = new System.Drawing.Size(40, 43);
            this.slider_img_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.slider_img_1.TabIndex = 14;
            this.slider_img_1.TabStop = false;
            this.slider_img_1.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // slider_img_night
            // 
            this.slider_img_night.BackColor = System.Drawing.Color.Transparent;
            this.slider_img_night.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.anim_vslide.SetDecoration(this.slider_img_night, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_img_night, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_img_night, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_img_night, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_img_night, BunifuAnimatorNS.DecorationType.None);
            this.slider_img_night.Image = ((System.Drawing.Image)(resources.GetObject("slider_img_night.Image")));
            this.slider_img_night.Location = new System.Drawing.Point(26, 58);
            this.slider_img_night.Name = "slider_img_night";
            this.slider_img_night.Size = new System.Drawing.Size(206, 59);
            this.slider_img_night.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.slider_img_night.TabIndex = 14;
            this.slider_img_night.TabStop = false;
            this.slider_img_night.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // slider_img_n_min
            // 
            this.slider_img_n_min.BackColor = System.Drawing.Color.Transparent;
            this.slider_img_n_min.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.anim_vslide.SetDecoration(this.slider_img_n_min, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_img_n_min, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_img_n_min, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_img_n_min, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_img_n_min, BunifuAnimatorNS.DecorationType.None);
            this.slider_img_n_min.Image = ((System.Drawing.Image)(resources.GetObject("slider_img_n_min.Image")));
            this.slider_img_n_min.Location = new System.Drawing.Point(10, 52);
            this.slider_img_n_min.Name = "slider_img_n_min";
            this.slider_img_n_min.Size = new System.Drawing.Size(67, 71);
            this.slider_img_n_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.slider_img_n_min.TabIndex = 20;
            this.slider_img_n_min.TabStop = false;
            // 
            // slider_btn_1
            // 
            this.slider_btn_1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slider_btn_1.BorderRadius = 0;
            this.slider_btn_1.ButtonText = "      Inicio";
            this.slider_btn_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_alt.SetDecoration(this.slider_btn_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_btn_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_btn_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_btn_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.slider_btn_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_btn_1.DisabledColor = System.Drawing.Color.Gray;
            this.slider_btn_1.Font = new System.Drawing.Font("Circular Std Book", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slider_btn_1.Iconcolor = System.Drawing.Color.Transparent;
            this.slider_btn_1.Iconimage = ((System.Drawing.Image)(resources.GetObject("slider_btn_1.Iconimage")));
            this.slider_btn_1.Iconimage_right = null;
            this.slider_btn_1.Iconimage_right_Selected = null;
            this.slider_btn_1.Iconimage_Selected = null;
            this.slider_btn_1.IconMarginLeft = 0;
            this.slider_btn_1.IconMarginRight = 0;
            this.slider_btn_1.IconRightVisible = true;
            this.slider_btn_1.IconRightZoom = 0D;
            this.slider_btn_1.IconVisible = true;
            this.slider_btn_1.IconZoom = 90D;
            this.slider_btn_1.IsTab = false;
            this.slider_btn_1.Location = new System.Drawing.Point(9, 160);
            this.slider_btn_1.Name = "slider_btn_1";
            this.slider_btn_1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_1.OnHoverTextColor = System.Drawing.Color.White;
            this.slider_btn_1.selected = false;
            this.slider_btn_1.Size = new System.Drawing.Size(246, 61);
            this.slider_btn_1.TabIndex = 0;
            this.slider_btn_1.Text = "      Inicio";
            this.slider_btn_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.slider_btn_1.Textcolor = System.Drawing.Color.White;
            this.slider_btn_1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.slider_btn_1.Click += new System.EventHandler(this.b_menu_Click);
            // 
            // slider_btn_2
            // 
            this.slider_btn_2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slider_btn_2.BorderRadius = 0;
            this.slider_btn_2.ButtonText = "      Bebidas";
            this.slider_btn_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_alt.SetDecoration(this.slider_btn_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_btn_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_btn_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_btn_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.slider_btn_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_btn_2.DisabledColor = System.Drawing.Color.Gray;
            this.slider_btn_2.Font = new System.Drawing.Font("Circular Std Book", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slider_btn_2.Iconcolor = System.Drawing.Color.Transparent;
            this.slider_btn_2.Iconimage = ((System.Drawing.Image)(resources.GetObject("slider_btn_2.Iconimage")));
            this.slider_btn_2.Iconimage_right = null;
            this.slider_btn_2.Iconimage_right_Selected = null;
            this.slider_btn_2.Iconimage_Selected = null;
            this.slider_btn_2.IconMarginLeft = 0;
            this.slider_btn_2.IconMarginRight = 0;
            this.slider_btn_2.IconRightVisible = true;
            this.slider_btn_2.IconRightZoom = 0D;
            this.slider_btn_2.IconVisible = true;
            this.slider_btn_2.IconZoom = 90D;
            this.slider_btn_2.IsTab = false;
            this.slider_btn_2.Location = new System.Drawing.Point(9, 222);
            this.slider_btn_2.Name = "slider_btn_2";
            this.slider_btn_2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_2.OnHoverTextColor = System.Drawing.Color.White;
            this.slider_btn_2.selected = false;
            this.slider_btn_2.Size = new System.Drawing.Size(246, 61);
            this.slider_btn_2.TabIndex = 0;
            this.slider_btn_2.Text = "      Bebidas";
            this.slider_btn_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.slider_btn_2.Textcolor = System.Drawing.Color.White;
            this.slider_btn_2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.slider_btn_2.Click += new System.EventHandler(this.b_drinks_Click);
            // 
            // slider_btn_logout
            // 
            this.slider_btn_logout.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.slider_btn_logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_logout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slider_btn_logout.BorderRadius = 0;
            this.slider_btn_logout.ButtonText = "      Logout";
            this.slider_btn_logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_alt.SetDecoration(this.slider_btn_logout, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_btn_logout, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_btn_logout, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_btn_logout, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.slider_btn_logout, BunifuAnimatorNS.DecorationType.None);
            this.slider_btn_logout.DisabledColor = System.Drawing.Color.Gray;
            this.slider_btn_logout.Font = new System.Drawing.Font("Circular Std Book", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slider_btn_logout.Iconcolor = System.Drawing.Color.Transparent;
            this.slider_btn_logout.Iconimage = ((System.Drawing.Image)(resources.GetObject("slider_btn_logout.Iconimage")));
            this.slider_btn_logout.Iconimage_right = null;
            this.slider_btn_logout.Iconimage_right_Selected = ((System.Drawing.Image)(resources.GetObject("slider_btn_logout.Iconimage_right_Selected")));
            this.slider_btn_logout.Iconimage_Selected = ((System.Drawing.Image)(resources.GetObject("slider_btn_logout.Iconimage_Selected")));
            this.slider_btn_logout.IconMarginLeft = 0;
            this.slider_btn_logout.IconMarginRight = 0;
            this.slider_btn_logout.IconRightVisible = true;
            this.slider_btn_logout.IconRightZoom = 0D;
            this.slider_btn_logout.IconVisible = true;
            this.slider_btn_logout.IconZoom = 90D;
            this.slider_btn_logout.IsTab = false;
            this.slider_btn_logout.Location = new System.Drawing.Point(9, 589);
            this.slider_btn_logout.Name = "slider_btn_logout";
            this.slider_btn_logout.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_logout.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_logout.OnHoverTextColor = System.Drawing.Color.White;
            this.slider_btn_logout.selected = false;
            this.slider_btn_logout.Size = new System.Drawing.Size(246, 61);
            this.slider_btn_logout.TabIndex = 19;
            this.slider_btn_logout.Text = "      Logout";
            this.slider_btn_logout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.slider_btn_logout.Textcolor = System.Drawing.Color.White;
            this.slider_btn_logout.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // slider_btn_4
            // 
            this.slider_btn_4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slider_btn_4.BorderRadius = 0;
            this.slider_btn_4.ButtonText = "      Alimentos";
            this.slider_btn_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_alt.SetDecoration(this.slider_btn_4, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_btn_4, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_btn_4, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_btn_4, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.slider_btn_4, BunifuAnimatorNS.DecorationType.None);
            this.slider_btn_4.DisabledColor = System.Drawing.Color.Gray;
            this.slider_btn_4.Font = new System.Drawing.Font("Circular Std Book", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slider_btn_4.Iconcolor = System.Drawing.Color.Transparent;
            this.slider_btn_4.Iconimage = ((System.Drawing.Image)(resources.GetObject("slider_btn_4.Iconimage")));
            this.slider_btn_4.Iconimage_right = null;
            this.slider_btn_4.Iconimage_right_Selected = null;
            this.slider_btn_4.Iconimage_Selected = null;
            this.slider_btn_4.IconMarginLeft = 0;
            this.slider_btn_4.IconMarginRight = 0;
            this.slider_btn_4.IconRightVisible = true;
            this.slider_btn_4.IconRightZoom = 0D;
            this.slider_btn_4.IconVisible = true;
            this.slider_btn_4.IconZoom = 90D;
            this.slider_btn_4.IsTab = false;
            this.slider_btn_4.Location = new System.Drawing.Point(9, 346);
            this.slider_btn_4.Name = "slider_btn_4";
            this.slider_btn_4.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_4.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_4.OnHoverTextColor = System.Drawing.Color.White;
            this.slider_btn_4.selected = false;
            this.slider_btn_4.Size = new System.Drawing.Size(246, 61);
            this.slider_btn_4.TabIndex = 19;
            this.slider_btn_4.Text = "      Alimentos";
            this.slider_btn_4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.slider_btn_4.Textcolor = System.Drawing.Color.White;
            this.slider_btn_4.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slider_btn_4.Click += new System.EventHandler(this.slider_btn_4_Click);
            // 
            // slider_btn_3
            // 
            this.slider_btn_3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.slider_btn_3.BorderRadius = 0;
            this.slider_btn_3.ButtonText = "      Cervezas";
            this.slider_btn_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_alt.SetDecoration(this.slider_btn_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_btn_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_btn_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_btn_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.slider_btn_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_btn_3.DisabledColor = System.Drawing.Color.Gray;
            this.slider_btn_3.Font = new System.Drawing.Font("Circular Std Book", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slider_btn_3.Iconcolor = System.Drawing.Color.Transparent;
            this.slider_btn_3.Iconimage = ((System.Drawing.Image)(resources.GetObject("slider_btn_3.Iconimage")));
            this.slider_btn_3.Iconimage_right = null;
            this.slider_btn_3.Iconimage_right_Selected = null;
            this.slider_btn_3.Iconimage_Selected = null;
            this.slider_btn_3.IconMarginLeft = 0;
            this.slider_btn_3.IconMarginRight = 0;
            this.slider_btn_3.IconRightVisible = true;
            this.slider_btn_3.IconRightZoom = 0D;
            this.slider_btn_3.IconVisible = true;
            this.slider_btn_3.IconZoom = 90D;
            this.slider_btn_3.IsTab = false;
            this.slider_btn_3.Location = new System.Drawing.Point(9, 284);
            this.slider_btn_3.Name = "slider_btn_3";
            this.slider_btn_3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_btn_3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.slider_btn_3.OnHoverTextColor = System.Drawing.Color.White;
            this.slider_btn_3.selected = false;
            this.slider_btn_3.Size = new System.Drawing.Size(246, 61);
            this.slider_btn_3.TabIndex = 19;
            this.slider_btn_3.Text = "      Cervezas";
            this.slider_btn_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.slider_btn_3.Textcolor = System.Drawing.Color.White;
            this.slider_btn_3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            // 
            // slider_pnl_3
            // 
            this.slider_pnl_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.anim_vslide.SetDecoration(this.slider_pnl_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_pnl_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_pnl_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_pnl_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_pnl_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_pnl_3.Location = new System.Drawing.Point(-3, 284);
            this.slider_pnl_3.Name = "slider_pnl_3";
            this.slider_pnl_3.Size = new System.Drawing.Size(14, 60);
            this.slider_pnl_3.TabIndex = 18;
            // 
            // slider_div_2
            // 
            this.slider_div_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.slider_div_2.BackColor = System.Drawing.Color.Transparent;
            this.anim_vslide.SetDecoration(this.slider_div_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_div_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_div_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_div_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_div_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_div_2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.slider_div_2.LineThickness = 1;
            this.slider_div_2.Location = new System.Drawing.Point(51, 661);
            this.slider_div_2.Name = "slider_div_2";
            this.slider_div_2.Size = new System.Drawing.Size(159, 35);
            this.slider_div_2.TabIndex = 14;
            this.slider_div_2.Transparency = 255;
            this.slider_div_2.Vertical = false;
            // 
            // slider_div_1
            // 
            this.slider_div_1.BackColor = System.Drawing.Color.Transparent;
            this.anim_vslide.SetDecoration(this.slider_div_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_div_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_div_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_div_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_div_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_div_1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.slider_div_1.LineThickness = 1;
            this.slider_div_1.Location = new System.Drawing.Point(51, 123);
            this.slider_div_1.Name = "slider_div_1";
            this.slider_div_1.Size = new System.Drawing.Size(159, 35);
            this.slider_div_1.TabIndex = 4;
            this.slider_div_1.Transparency = 255;
            this.slider_div_1.Vertical = false;
            // 
            // slider_pnl_left
            // 
            this.slider_pnl_left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.slider_pnl_left.Controls.Add(this.slider_pnl_drinks);
            this.slider_pnl_left.Controls.Add(this.slider_pnl_mmenu);
            this.slider_pnl_left.Controls.Add(this.slider_pnl_logout);
            this.slider_pnl_left.Controls.Add(this.panel2);
            this.anim_vslide.SetDecoration(this.slider_pnl_left, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_pnl_left, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_pnl_left, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_pnl_left, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_pnl_left, BunifuAnimatorNS.DecorationType.None);
            this.slider_pnl_left.Dock = System.Windows.Forms.DockStyle.Left;
            this.slider_pnl_left.Location = new System.Drawing.Point(0, 0);
            this.slider_pnl_left.Name = "slider_pnl_left";
            this.slider_pnl_left.Size = new System.Drawing.Size(10, 741);
            this.slider_pnl_left.TabIndex = 1;
            // 
            // slider_pnl_drinks
            // 
            this.slider_pnl_drinks.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.anim_vslide.SetDecoration(this.slider_pnl_drinks, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_pnl_drinks, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_pnl_drinks, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_pnl_drinks, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_pnl_drinks, BunifuAnimatorNS.DecorationType.None);
            this.slider_pnl_drinks.Location = new System.Drawing.Point(0, 222);
            this.slider_pnl_drinks.Name = "slider_pnl_drinks";
            this.slider_pnl_drinks.Size = new System.Drawing.Size(14, 60);
            this.slider_pnl_drinks.TabIndex = 0;
            // 
            // slider_pnl_mmenu
            // 
            this.slider_pnl_mmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.anim_vslide.SetDecoration(this.slider_pnl_mmenu, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_pnl_mmenu, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_pnl_mmenu, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_pnl_mmenu, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_pnl_mmenu, BunifuAnimatorNS.DecorationType.None);
            this.slider_pnl_mmenu.Location = new System.Drawing.Point(0, 160);
            this.slider_pnl_mmenu.Name = "slider_pnl_mmenu";
            this.slider_pnl_mmenu.Size = new System.Drawing.Size(14, 60);
            this.slider_pnl_mmenu.TabIndex = 0;
            // 
            // slider_pnl_logout
            // 
            this.slider_pnl_logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.slider_pnl_logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.anim_vslide.SetDecoration(this.slider_pnl_logout, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_pnl_logout, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_pnl_logout, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_pnl_logout, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_pnl_logout, BunifuAnimatorNS.DecorationType.None);
            this.slider_pnl_logout.Location = new System.Drawing.Point(0, 589);
            this.slider_pnl_logout.Name = "slider_pnl_logout";
            this.slider_pnl_logout.Size = new System.Drawing.Size(14, 60);
            this.slider_pnl_logout.TabIndex = 18;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.anim_vslide.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Location = new System.Drawing.Point(0, 346);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(14, 60);
            this.panel2.TabIndex = 18;
            // 
            // slider_img_powered
            // 
            this.slider_img_powered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.slider_img_powered.BackColor = System.Drawing.Color.Transparent;
            this.slider_img_powered.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.slider_img_powered, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.slider_img_powered, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.slider_img_powered, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.slider_img_powered, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.slider_img_powered, BunifuAnimatorNS.DecorationType.None);
            this.slider_img_powered.Image = ((System.Drawing.Image)(resources.GetObject("slider_img_powered.Image")));
            this.slider_img_powered.Location = new System.Drawing.Point(63, 687);
            this.slider_img_powered.Name = "slider_img_powered";
            this.slider_img_powered.Size = new System.Drawing.Size(132, 42);
            this.slider_img_powered.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.slider_img_powered.TabIndex = 14;
            this.slider_img_powered.TabStop = false;
            this.slider_img_powered.Click += new System.EventHandler(this.slider_img_powered_Click);
            // 
            // drag_mainpnl
            // 
            this.drag_mainpnl.Fixed = true;
            this.drag_mainpnl.Horizontal = true;
            this.drag_mainpnl.TargetControl = null;
            this.drag_mainpnl.Vertical = true;
            // 
            // drag_mainmenu
            // 
            this.drag_mainmenu.Fixed = true;
            this.drag_mainmenu.Horizontal = true;
            this.drag_mainmenu.TargetControl = this.main_pnl_drag;
            this.drag_mainmenu.Vertical = true;
            // 
            // main_pnl_drag
            // 
            this.anim_vslide.SetDecoration(this.main_pnl_drag, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_pnl_drag, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_pnl_drag, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_pnl_drag, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.main_pnl_drag, BunifuAnimatorNS.DecorationType.None);
            this.main_pnl_drag.Location = new System.Drawing.Point(0, 0);
            this.main_pnl_drag.Name = "main_pnl_drag";
            this.main_pnl_drag.Size = new System.Drawing.Size(968, 30);
            this.main_pnl_drag.TabIndex = 23;
            // 
            // drag_pnl_bottom
            // 
            this.drag_pnl_bottom.Fixed = true;
            this.drag_pnl_bottom.Horizontal = true;
            this.drag_pnl_bottom.TargetControl = this.bottom_pnl;
            this.drag_pnl_bottom.Vertical = true;
            // 
            // bottom_pnl
            // 
            this.bottom_pnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.bottom_pnl.Controls.Add(this.bottom_txt_user);
            this.bottom_pnl.Controls.Add(this.bottom_img_client);
            this.anim_vslide.SetDecoration(this.bottom_pnl, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.bottom_pnl, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.bottom_pnl, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.bottom_pnl, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.bottom_pnl, BunifuAnimatorNS.DecorationType.None);
            this.bottom_pnl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottom_pnl.Location = new System.Drawing.Point(0, 649);
            this.bottom_pnl.Name = "bottom_pnl";
            this.bottom_pnl.Size = new System.Drawing.Size(1055, 92);
            this.bottom_pnl.TabIndex = 12;
            // 
            // bottom_txt_user
            // 
            this.bottom_txt_user.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bottom_txt_user.AutoSize = true;
            this.anim_alt.SetDecoration(this.bottom_txt_user, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.bottom_txt_user, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.bottom_txt_user, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.bottom_txt_user, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.bottom_txt_user, BunifuAnimatorNS.DecorationType.None);
            this.bottom_txt_user.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 12F);
            this.bottom_txt_user.ForeColor = System.Drawing.Color.White;
            this.bottom_txt_user.Location = new System.Drawing.Point(370, 64);
            this.bottom_txt_user.Name = "bottom_txt_user";
            this.bottom_txt_user.Size = new System.Drawing.Size(175, 19);
            this.bottom_txt_user.TabIndex = 18;
            this.bottom_txt_user.Text = "Usuario: Fernando Herrera";
            // 
            // bottom_img_client
            // 
            this.bottom_img_client.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bottom_img_client.BackColor = System.Drawing.Color.Transparent;
            this.bottom_img_client.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.bottom_img_client, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.bottom_img_client, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.bottom_img_client, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.bottom_img_client, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.bottom_img_client, BunifuAnimatorNS.DecorationType.None);
            this.bottom_img_client.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.bottom_img_client.Image = ((System.Drawing.Image)(resources.GetObject("bottom_img_client.Image")));
            this.bottom_img_client.ImageActive = null;
            this.bottom_img_client.Location = new System.Drawing.Point(374, -9);
            this.bottom_img_client.Name = "bottom_img_client";
            this.bottom_img_client.Size = new System.Drawing.Size(165, 89);
            this.bottom_img_client.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bottom_img_client.TabIndex = 17;
            this.bottom_img_client.TabStop = false;
            this.bottom_img_client.Zoom = 10;
            // 
            // slider_anim1
            // 
            this.slider_anim1.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.slider_anim1.Cursor = null;
            animation7.AnimateOnlyDifferences = true;
            animation7.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.BlindCoeff")));
            animation7.LeafCoeff = 0F;
            animation7.MaxTime = 1F;
            animation7.MinTime = 0F;
            animation7.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.MosaicCoeff")));
            animation7.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation7.MosaicShift")));
            animation7.MosaicSize = 0;
            animation7.Padding = new System.Windows.Forms.Padding(0);
            animation7.RotateCoeff = 0F;
            animation7.RotateLimit = 0F;
            animation7.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.ScaleCoeff")));
            animation7.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.SlideCoeff")));
            animation7.TimeCoeff = 0F;
            animation7.TransparencyCoeff = 0F;
            this.slider_anim1.DefaultAnimation = animation7;
            // 
            // main_panel
            // 
            this.main_panel.BackColor = System.Drawing.SystemColors.Control;
            this.main_panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.main_panel.Controls.Add(this.main_pnl_drag);
            this.main_panel.Controls.Add(this.main_txt_3);
            this.main_panel.Controls.Add(this.main_txt_2);
            this.main_panel.Controls.Add(this.main_txt_1);
            this.main_panel.Controls.Add(this.main_img_3);
            this.main_panel.Controls.Add(this.main_img_2);
            this.main_panel.Controls.Add(this.main_img_1);
            this.main_panel.Controls.Add(this.pnl1_spt_1);
            this.main_panel.Controls.Add(this.main_banner_txt_1);
            this.main_panel.Controls.Add(this.main_banner_img_client);
            this.main_panel.Controls.Add(this.gui_top_11);
            this.main_panel.Controls.Add(this.gui_uc_drinks1);
            this.anim_vslide.SetDecoration(this.main_panel, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_panel, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_panel, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_panel, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.main_panel, BunifuAnimatorNS.DecorationType.None);
            this.main_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_panel.Location = new System.Drawing.Point(0, 0);
            this.main_panel.Name = "main_panel";
            this.main_panel.Size = new System.Drawing.Size(1055, 741);
            this.main_panel.TabIndex = 17;
            // 
            // main_txt_3
            // 
            this.main_txt_3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.main_txt_3.AutoSize = true;
            this.main_txt_3.BackColor = System.Drawing.Color.Transparent;
            this.anim_alt.SetDecoration(this.main_txt_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_txt_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.main_txt_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_txt_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_txt_3, BunifuAnimatorNS.DecorationType.None);
            this.main_txt_3.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.main_txt_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(80)))), ((int)(((byte)(87)))));
            this.main_txt_3.Location = new System.Drawing.Point(747, 459);
            this.main_txt_3.Name = "main_txt_3";
            this.main_txt_3.Size = new System.Drawing.Size(111, 33);
            this.main_txt_3.TabIndex = 22;
            this.main_txt_3.Text = "Top Drink";
            // 
            // main_txt_2
            // 
            this.main_txt_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.main_txt_2.AutoSize = true;
            this.main_txt_2.BackColor = System.Drawing.Color.Transparent;
            this.anim_alt.SetDecoration(this.main_txt_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_txt_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.main_txt_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_txt_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_txt_2, BunifuAnimatorNS.DecorationType.None);
            this.main_txt_2.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.main_txt_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(80)))), ((int)(((byte)(87)))));
            this.main_txt_2.Location = new System.Drawing.Point(452, 459);
            this.main_txt_2.Name = "main_txt_2";
            this.main_txt_2.Size = new System.Drawing.Size(104, 33);
            this.main_txt_2.TabIndex = 22;
            this.main_txt_2.Text = "Top Beer";
            // 
            // main_txt_1
            // 
            this.main_txt_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.main_txt_1.AutoSize = true;
            this.main_txt_1.BackColor = System.Drawing.Color.Transparent;
            this.anim_alt.SetDecoration(this.main_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.main_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.main_txt_1.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.main_txt_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(80)))), ((int)(((byte)(87)))));
            this.main_txt_1.Location = new System.Drawing.Point(133, 459);
            this.main_txt_1.Name = "main_txt_1";
            this.main_txt_1.Size = new System.Drawing.Size(125, 33);
            this.main_txt_1.TabIndex = 22;
            this.main_txt_1.Text = "+ Vendidos";
            // 
            // main_img_3
            // 
            this.main_img_3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.main_img_3.BackColor = System.Drawing.Color.Transparent;
            this.main_img_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.main_img_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_img_3, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_img_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_img_3, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.main_img_3, BunifuAnimatorNS.DecorationType.None);
            this.main_img_3.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.main_img_3.Image = ((System.Drawing.Image)(resources.GetObject("main_img_3.Image")));
            this.main_img_3.ImageActive = ((System.Drawing.Image)(resources.GetObject("main_img_3.ImageActive")));
            this.main_img_3.Location = new System.Drawing.Point(692, 244);
            this.main_img_3.Name = "main_img_3";
            this.main_img_3.Size = new System.Drawing.Size(212, 208);
            this.main_img_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.main_img_3.TabIndex = 21;
            this.main_img_3.TabStop = false;
            this.main_img_3.Zoom = 10;
            // 
            // main_img_2
            // 
            this.main_img_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.main_img_2.BackColor = System.Drawing.Color.Transparent;
            this.main_img_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.main_img_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_img_2, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_img_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_img_2, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.main_img_2, BunifuAnimatorNS.DecorationType.None);
            this.main_img_2.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.main_img_2.Image = ((System.Drawing.Image)(resources.GetObject("main_img_2.Image")));
            this.main_img_2.ImageActive = ((System.Drawing.Image)(resources.GetObject("main_img_2.ImageActive")));
            this.main_img_2.Location = new System.Drawing.Point(393, 244);
            this.main_img_2.Name = "main_img_2";
            this.main_img_2.Size = new System.Drawing.Size(212, 208);
            this.main_img_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.main_img_2.TabIndex = 21;
            this.main_img_2.TabStop = false;
            this.main_img_2.Zoom = 10;
            // 
            // main_img_1
            // 
            this.main_img_1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.main_img_1.BackColor = System.Drawing.Color.Transparent;
            this.main_img_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.main_img_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_img_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_img_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_img_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.main_img_1, BunifuAnimatorNS.DecorationType.None);
            this.main_img_1.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.main_img_1.Image = ((System.Drawing.Image)(resources.GetObject("main_img_1.Image")));
            this.main_img_1.ImageActive = ((System.Drawing.Image)(resources.GetObject("main_img_1.ImageActive")));
            this.main_img_1.Location = new System.Drawing.Point(92, 244);
            this.main_img_1.Name = "main_img_1";
            this.main_img_1.Size = new System.Drawing.Size(212, 208);
            this.main_img_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.main_img_1.TabIndex = 21;
            this.main_img_1.TabStop = false;
            this.main_img_1.Zoom = 10;
            this.main_img_1.Click += new System.EventHandler(this.main_img_1_Click);
            // 
            // pnl1_spt_1
            // 
            this.pnl1_spt_1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnl1_spt_1.BackColor = System.Drawing.Color.Transparent;
            this.anim_vslide.SetDecoration(this.pnl1_spt_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.pnl1_spt_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.pnl1_spt_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.pnl1_spt_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.pnl1_spt_1, BunifuAnimatorNS.DecorationType.None);
            this.pnl1_spt_1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.pnl1_spt_1.LineThickness = 1;
            this.pnl1_spt_1.Location = new System.Drawing.Point(350, 123);
            this.pnl1_spt_1.Name = "pnl1_spt_1";
            this.pnl1_spt_1.Size = new System.Drawing.Size(429, 35);
            this.pnl1_spt_1.TabIndex = 18;
            this.pnl1_spt_1.Transparency = 255;
            this.pnl1_spt_1.Vertical = false;
            // 
            // main_banner_txt_1
            // 
            this.main_banner_txt_1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.main_banner_txt_1.AutoSize = true;
            this.main_banner_txt_1.BackColor = System.Drawing.Color.Transparent;
            this.anim_alt.SetDecoration(this.main_banner_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_banner_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.main_banner_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_banner_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_banner_txt_1, BunifuAnimatorNS.DecorationType.None);
            this.main_banner_txt_1.Font = new System.Drawing.Font("Circular Std Book", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.main_banner_txt_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.main_banner_txt_1.Location = new System.Drawing.Point(281, 67);
            this.main_banner_txt_1.Name = "main_banner_txt_1";
            this.main_banner_txt_1.Size = new System.Drawing.Size(623, 44);
            this.main_banner_txt_1.TabIndex = 18;
            this.main_banner_txt_1.Text = "Bienvenido a NightLifeX, Privé CUU!";
            // 
            // main_banner_img_client
            // 
            this.main_banner_img_client.BackColor = System.Drawing.Color.Transparent;
            this.main_banner_img_client.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.main_banner_img_client, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.main_banner_img_client, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.main_banner_img_client, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.main_banner_img_client, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.main_banner_img_client, BunifuAnimatorNS.DecorationType.None);
            this.main_banner_img_client.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.main_banner_img_client.Image = ((System.Drawing.Image)(resources.GetObject("main_banner_img_client.Image")));
            this.main_banner_img_client.ImageActive = null;
            this.main_banner_img_client.Location = new System.Drawing.Point(0, -4);
            this.main_banner_img_client.Name = "main_banner_img_client";
            this.main_banner_img_client.Size = new System.Drawing.Size(275, 159);
            this.main_banner_img_client.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.main_banner_img_client.TabIndex = 18;
            this.main_banner_img_client.TabStop = false;
            this.main_banner_img_client.Zoom = 10;
            // 
            // gui_top_11
            // 
            this.anim_alt.SetDecoration(this.gui_top_11, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.gui_top_11, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.gui_top_11, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.gui_top_11, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.gui_top_11, BunifuAnimatorNS.DecorationType.None);
            this.gui_top_11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gui_top_11.Location = new System.Drawing.Point(0, 0);
            this.gui_top_11.Name = "gui_top_11";
            this.gui_top_11.Size = new System.Drawing.Size(1055, 741);
            this.gui_top_11.TabIndex = 24;
            this.gui_top_11.Load += new System.EventHandler(this.gui_top_11_Load);
            // 
            // gui_uc_drinks1
            // 
            this.anim_alt.SetDecoration(this.gui_uc_drinks1, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.gui_uc_drinks1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.gui_uc_drinks1, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.gui_uc_drinks1, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this.gui_uc_drinks1, BunifuAnimatorNS.DecorationType.None);
            this.gui_uc_drinks1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gui_uc_drinks1.Location = new System.Drawing.Point(0, 0);
            this.gui_uc_drinks1.Name = "gui_uc_drinks1";
            this.gui_uc_drinks1.Size = new System.Drawing.Size(1055, 741);
            this.gui_uc_drinks1.TabIndex = 25;
            // 
            // b_min
            // 
            this.b_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_min.BackColor = System.Drawing.SystemColors.Control;
            this.b_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.b_min, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.b_min, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.b_min, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.b_min, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.b_min, BunifuAnimatorNS.DecorationType.None);
            this.b_min.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.b_min.Image = ((System.Drawing.Image)(resources.GetObject("b_min.Image")));
            this.b_min.ImageActive = null;
            this.b_min.Location = new System.Drawing.Point(949, 10);
            this.b_min.Name = "b_min";
            this.b_min.Size = new System.Drawing.Size(20, 21);
            this.b_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.b_min.TabIndex = 4;
            this.b_min.TabStop = false;
            this.b_min.Zoom = 10;
            this.b_min.Click += new System.EventHandler(this.b_min_Click);
            // 
            // b_max
            // 
            this.b_max.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_max.BackColor = System.Drawing.SystemColors.Control;
            this.b_max.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.b_max, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.b_max, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.b_max, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.b_max, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.b_max, BunifuAnimatorNS.DecorationType.None);
            this.b_max.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.b_max.Image = ((System.Drawing.Image)(resources.GetObject("b_max.Image")));
            this.b_max.ImageActive = null;
            this.b_max.Location = new System.Drawing.Point(978, 10);
            this.b_max.Name = "b_max";
            this.b_max.Size = new System.Drawing.Size(20, 21);
            this.b_max.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.b_max.TabIndex = 5;
            this.b_max.TabStop = false;
            this.b_max.Zoom = 10;
            this.b_max.Click += new System.EventHandler(this.b_max_Click);
            // 
            // b_off
            // 
            this.b_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_off.BackColor = System.Drawing.SystemColors.Control;
            this.b_off.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_vslide.SetDecoration(this.b_off, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.b_off, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.b_off, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.b_off, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.b_off, BunifuAnimatorNS.DecorationType.None);
            this.b_off.ErrorImage = global::NightLifeX.Properties.Resources.image_2;
            this.b_off.Image = ((System.Drawing.Image)(resources.GetObject("b_off.Image")));
            this.b_off.ImageActive = null;
            this.b_off.Location = new System.Drawing.Point(1008, 10);
            this.b_off.Name = "b_off";
            this.b_off.Size = new System.Drawing.Size(20, 21);
            this.b_off.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.b_off.TabIndex = 3;
            this.b_off.TabStop = false;
            this.b_off.Zoom = 10;
            this.b_off.Click += new System.EventHandler(this.b_off_Click);
            // 
            // gui_card
            // 
            this.gui_card.BackColor = System.Drawing.Color.White;
            this.gui_card.BorderRadius = 5;
            this.gui_card.BottomSahddow = true;
            this.gui_card.color = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(185)))), ((int)(((byte)(84)))));
            this.gui_card.Controls.Add(this.b_off);
            this.gui_card.Controls.Add(this.b_max);
            this.gui_card.Controls.Add(this.b_min);
            this.gui_card.Controls.Add(this.bottom_pnl);
            this.gui_card.Controls.Add(this.main_panel);
            this.anim_vslide.SetDecoration(this.gui_card, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this.gui_card, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this.gui_card, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim2.SetDecoration(this.gui_card, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this.gui_card, BunifuAnimatorNS.DecorationType.None);
            this.gui_card.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gui_card.LeftSahddow = false;
            this.gui_card.Location = new System.Drawing.Point(255, 0);
            this.gui_card.Name = "gui_card";
            this.gui_card.RightSahddow = true;
            this.gui_card.ShadowDepth = 20;
            this.gui_card.Size = new System.Drawing.Size(1055, 741);
            this.gui_card.TabIndex = 6;
            // 
            // slider_anim2
            // 
            this.slider_anim2.AnimationType = BunifuAnimatorNS.AnimationType.VertSlide;
            this.slider_anim2.Cursor = null;
            animation6.AnimateOnlyDifferences = true;
            animation6.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.BlindCoeff")));
            animation6.LeafCoeff = 0F;
            animation6.MaxTime = 1F;
            animation6.MinTime = 0F;
            animation6.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.MosaicCoeff")));
            animation6.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation6.MosaicShift")));
            animation6.MosaicSize = 0;
            animation6.Padding = new System.Windows.Forms.Padding(0);
            animation6.RotateCoeff = 0F;
            animation6.RotateLimit = 0F;
            animation6.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.ScaleCoeff")));
            animation6.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.SlideCoeff")));
            animation6.TimeCoeff = 0F;
            animation6.TransparencyCoeff = 0F;
            this.slider_anim2.DefaultAnimation = animation6;
            // 
            // anim_vslide
            // 
            this.anim_vslide.AnimationType = BunifuAnimatorNS.AnimationType.VertSlide;
            this.anim_vslide.Cursor = null;
            animation9.AnimateOnlyDifferences = true;
            animation9.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation9.BlindCoeff")));
            animation9.LeafCoeff = 0F;
            animation9.MaxTime = 1F;
            animation9.MinTime = 0F;
            animation9.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation9.MosaicCoeff")));
            animation9.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation9.MosaicShift")));
            animation9.MosaicSize = 0;
            animation9.Padding = new System.Windows.Forms.Padding(0);
            animation9.RotateCoeff = 0F;
            animation9.RotateLimit = 0F;
            animation9.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation9.ScaleCoeff")));
            animation9.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation9.SlideCoeff")));
            animation9.TimeCoeff = 0F;
            animation9.TransparencyCoeff = 0F;
            this.anim_vslide.DefaultAnimation = animation9;
            this.anim_vslide.MaxAnimationTime = 1000;
            // 
            // anim_fade
            // 
            this.anim_fade.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.anim_fade.Cursor = null;
            animation8.AnimateOnlyDifferences = true;
            animation8.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation8.BlindCoeff")));
            animation8.LeafCoeff = 0F;
            animation8.MaxTime = 1F;
            animation8.MinTime = 0F;
            animation8.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation8.MosaicCoeff")));
            animation8.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation8.MosaicShift")));
            animation8.MosaicSize = 0;
            animation8.Padding = new System.Windows.Forms.Padding(0);
            animation8.RotateCoeff = 0F;
            animation8.RotateLimit = 0F;
            animation8.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation8.ScaleCoeff")));
            animation8.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation8.SlideCoeff")));
            animation8.TimeCoeff = 0F;
            animation8.TransparencyCoeff = 1F;
            this.anim_fade.DefaultAnimation = animation8;
            this.anim_fade.MaxAnimationTime = 1000;
            // 
            // anim_alt
            // 
            this.anim_alt.AnimationType = BunifuAnimatorNS.AnimationType.ScaleAndRotate;
            this.anim_alt.Cursor = null;
            animation10.AnimateOnlyDifferences = true;
            animation10.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation10.BlindCoeff")));
            animation10.LeafCoeff = 0F;
            animation10.MaxTime = 1F;
            animation10.MinTime = 0F;
            animation10.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation10.MosaicCoeff")));
            animation10.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation10.MosaicShift")));
            animation10.MosaicSize = 0;
            animation10.Padding = new System.Windows.Forms.Padding(30);
            animation10.RotateCoeff = 0.5F;
            animation10.RotateLimit = 0.2F;
            animation10.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation10.ScaleCoeff")));
            animation10.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation10.SlideCoeff")));
            animation10.TimeCoeff = 0F;
            animation10.TransparencyCoeff = 0F;
            this.anim_alt.DefaultAnimation = animation10;
            this.anim_alt.MaxAnimationTime = 1000;
            // 
            // gui_root
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.ClientSize = new System.Drawing.Size(1310, 741);
            this.Controls.Add(this.gui_card);
            this.Controls.Add(this.slider_pnl);
            this.slider_anim2.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.slider_anim1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.anim_fade.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.anim_vslide.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.anim_alt.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "gui_root";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NightLifeX";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.gui_root_FormClosed);
            this.slider_pnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_night)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_n_min)).EndInit();
            this.slider_pnl_left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.slider_img_powered)).EndInit();
            this.bottom_pnl.ResumeLayout(false);
            this.bottom_pnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottom_img_client)).EndInit();
            this.main_panel.ResumeLayout(false);
            this.main_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_img_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_banner_img_client)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_off)).EndInit();
            this.gui_card.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuDragControl drag_slider;
        private System.Windows.Forms.Panel slider_pnl;
        private System.Windows.Forms.Panel slider_pnl_left;
        private System.Windows.Forms.Panel slider_pnl_mmenu;
        private Bunifu.Framework.UI.BunifuFlatButton slider_btn_1;
        private Bunifu.Framework.UI.BunifuSeparator slider_div_1;
        private Bunifu.Framework.UI.BunifuDragControl drag_mainpnl;
        private Bunifu.Framework.UI.BunifuFlatButton slider_btn_2;
        private Bunifu.Framework.UI.BunifuSeparator slider_div_2;
        private System.Windows.Forms.PictureBox slider_img_1;
        private Bunifu.Framework.UI.BunifuDragControl drag_mainmenu;
        private Bunifu.Framework.UI.BunifuFlatButton slider_btn_3;
        private Bunifu.Framework.UI.BunifuDragControl drag_pnl_bottom;
        private Bunifu.Framework.UI.BunifuFlatButton slider_btn_4;
        private System.Windows.Forms.PictureBox slider_img_powered;
        private Bunifu.Framework.UI.BunifuFlatButton slider_btn_logout;
        private System.Windows.Forms.Panel slider_pnl_logout;
        private BunifuAnimatorNS.BunifuTransition slider_anim1;
        private BunifuAnimatorNS.BunifuTransition slider_anim2;
        private System.Windows.Forms.PictureBox slider_img_night;
        private System.Windows.Forms.PictureBox slider_img_n_min;
        private BunifuAnimatorNS.BunifuTransition anim_vslide;
        private BunifuAnimatorNS.BunifuTransition anim_fade;
        private BunifuAnimatorNS.BunifuTransition anim_alt;
        private System.Windows.Forms.Panel slider_pnl_3;
        private System.Windows.Forms.Panel slider_pnl_drinks;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuCards gui_card;
        private Bunifu.Framework.UI.BunifuImageButton b_off;
        private Bunifu.Framework.UI.BunifuImageButton b_max;
        private Bunifu.Framework.UI.BunifuImageButton b_min;
        private System.Windows.Forms.Panel bottom_pnl;
        private System.Windows.Forms.Label bottom_txt_user;
        private Bunifu.Framework.UI.BunifuImageButton bottom_img_client;
        private System.Windows.Forms.Panel main_panel;
        private System.Windows.Forms.Panel main_pnl_drag;
        private System.Windows.Forms.Label main_txt_3;
        private System.Windows.Forms.Label main_txt_2;
        private System.Windows.Forms.Label main_txt_1;
        private Bunifu.Framework.UI.BunifuImageButton main_img_3;
        private Bunifu.Framework.UI.BunifuImageButton main_img_2;
        private Bunifu.Framework.UI.BunifuImageButton main_img_1;
        private Bunifu.Framework.UI.BunifuSeparator pnl1_spt_1;
        private System.Windows.Forms.Label main_banner_txt_1;
        private Bunifu.Framework.UI.BunifuImageButton main_banner_img_client;
        private gui_top_1 gui_top_11;
        private gui_uc_drinks gui_uc_drinks1;
    }
}

